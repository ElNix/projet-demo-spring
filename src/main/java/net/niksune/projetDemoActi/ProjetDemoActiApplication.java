package net.niksune.projetDemoActi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetDemoActiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetDemoActiApplication.class, args);
	}

}
