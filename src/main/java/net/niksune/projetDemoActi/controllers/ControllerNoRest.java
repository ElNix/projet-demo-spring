

package net.niksune.projetDemoActi.controllers;



import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/norest/")
public class ControllerNoRest {


    @GetMapping("")
    //@RequestMapping(value = "/norest")
    public String norest() {
        System.out.println("NO REST");

        return "Bien le coucou no rest !";
    }

    @GetMapping("ajout")
    public void ajout(HttpServletRequest request) {
        System.out.println("Ajout");

        request.getSession().setAttribute("hey","yo");
    }


    @GetMapping("lecture")
    public void lecture(HttpServletRequest request) {
        System.out.println("Lecture");

        System.out.println(request.getSession().getAttribute("hey"));
    }


}

