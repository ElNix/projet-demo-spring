package net.niksune.projetDemoActi.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/projetDemo/")
public class MainWebController {

    @GetMapping("")
    public String index() {
        return "Bien le coucou";
    }

    @GetMapping("test")
    public String test() {
        return "test";
    }

    @GetMapping("tests/{nombre}")
    public String getTestsNombre(@PathVariable("nombre") String nombre) {
        return "J'ai reçu le nombre "+nombre+" depuis l'url !";
    }

    @GetMapping("tests/{nombre}/{autreElement}")
    public String getTestsNombre(@PathVariable("nombre") String nombre, @PathVariable("autreElement") String autreELement) {
        return "J'ai reçu le nombre "+nombre+" depuis l'url !\nAinsi que cet autre élément : "+autreELement;
    }

}
